#!/bin/bash

echo "Starting nightly maintenance"

echo "Archiving nightly server backup"
mkdir -p ${PERSIST_PATH}/saves/backups
tar -czvf "${PERSIST_PATH}/saves/backups/$(date '+%Y-%m-%d_%H-%M-%S').tar.gz" ${PERSIST_PATH}/saves/current

echo "Deleting backups older than ${KEEP_DAYS} days"
find ${PERSIST_PATH}/saves/backups -mtime +14 -type f -delete

echo "Nightly mainenance complete"

# Always mark the job as succeeded, even if the server isn't up
true
