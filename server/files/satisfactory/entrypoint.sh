#!/bin/bash

prep_term()
{
    unset term_child_pid
    unset term_kill_needed
    trap 'handle_term' TERM INT
}

handle_term()
{
    if [ "${term_child_pid}" ]; then
      safe_shutdown
      kill -TERM "${term_child_pid}" 2>/dev/null
    else
      term_kill_needed="yes"
    fi
}

wait_term()
{
  term_child_pid=$!
    if [ "${term_kill_needed}" ]; then
      safe_shutdown
      kill -TERM "${term_child_pid}" 2>/dev/null
    fi
    wait ${term_child_pid} 2>/dev/null
    trap - TERM INT
    wait ${term_child_pid} 2>/dev/null
}


safe_shutdown() {
  # No rcon support, so just shut down
  echo "Shutting down"
}

prep_term

mkdir -p ${PERSIST_PATH}/saves/current
mkdir -p ${PERSIST_PATH}/.config/Epic/FactoryGame/Saved
ln -sf ${PERSIST_PATH}/saves/current ${PERSIST_PATH}/.config/Epic/FactoryGame/Saved/SaveGames

mkdir -p ${PERSIST_PATH}/config
mkdir -p ${INSTALL_PATH}/FactoryGame/Saved/Config
ln -sf ${PERSIST_PATH}/config ${INSTALL_PATH}/FactoryGame/Saved/Config/LinuxServer

export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${INSTALL_PATH}/linux64"

${INSTALL_PATH}/FactoryServer.sh -multihome=0.0.0.0 -ini:Engine:[HTTPServer.Listeners]:DefaultBindAddress=any &
wait_term
